package fr.afpa.cda.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HelloWorld
 */
public class HelloWorld extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HelloWorld() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("<h1>CDA</h1>");
		response.getWriter().append("<form action=\"afficheNom\">");
		response.getWriter().append("<label for = \"nom\">Nom</label>\r\n" + 
				"<input type=\"text\" id=\"nom\" name=\"nom\"/><br/>");
		
		response.getWriter().append("<label for = \"prenom\">Prenom : </label>\r\n" + 
				"<input type=\"text\" id=\"prenom\" name = \"prenom\" required=\"required\"><br>");
		
		
		response.getWriter().append("<button type=\"submit\">Valider</button>\r\n" + 
				"</form>");
		
		response.getWriter().append("<a href=http://localhost:8080/HelloWorld>retour</a>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
